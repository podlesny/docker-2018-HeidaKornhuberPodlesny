#!/usr/bin/env bash

set -e

tag=2018-HeidaKornhuberPodlesny
docker build \
    -t podlesny/dune-faultnetworks:${tag} 02-dune-faultnetworks

tmpdir=`mktemp -d`
docker run --rm -v $tmpdir:/bridge podlesny/dune-faultnetworks:${tag} \
    sh -c 'tar -C /dune-build/dune-faultnetworks/src -cf /bridge/dune-faultnetworks.tar \
               geofaultnetworks/geofaultnetwork \
               cantorfaultnetworks/cantorfaultnetwork \
               cantorfaultnetworks/cantorconvergence && \
           tar -C /dune-sources/dune-faultnetworks/src -rf /bridge/dune-faultnetworks.tar \
               geofaultnetworks/geofaultnetwork.parset \
               geofaultnetworks/results/ \
               cantorfaultnetworks/cantorfaultnetwork.parset \
               cantorfaultnetworks/cantorconvergence.parset \
               cantorfaultnetworks/results/ \
               data/ && \
           tar -C /usr/lib/x86_64-linux-gnu -h -rf /bridge/dune-faultnetworks.tar \
               libumfpack.so.5 \
               libspqr.so.2 \
               libldl.so.2 \
               libstdc++.so.6 && \
           tar -C /lib/x86_64-linux-gnu -h -rf /bridge/dune-faultnetworks.tar \
               ld-linux-x86-64.so.2 \
               libc.so.6 \
               libdl.so.2 \
               libgcc_s.so.1 \
               libm.so.6 \
               libpthread.so.0 \
               libz.so.1'
mv $tmpdir/dune-faultnetworks.tar .
rmdir $tmpdir
rm -rf dune-faultnetworks
mkdir dune-faultnetworks
tar -xvf dune-faultnetworks.tar -C dune-faultnetworks
